# ChickTechAIWorkshopNCSU

This is the tutorial for the ChickTech AI Workshop for the Kickoff Event at Meredith on 12/7/19-12/8/19

## Machine Setup

### Install Anaconda
Installing [Anaconda](https://docs.anaconda.com/anaconda/install/) will provide access to Jupyter notebooks, Python 3.7, and the required libraries for running the tutorial.

Note: This has already been on the lab machines. If you decide to play with this on a different machine that cannot support the full anaconda installation, instead install miniconda
Choose the following options
* "Just me" installation
* Do not add to path
* Register Anaconda as my default Python 3.7

### Install SourceTree
[SourceTree](https://confluence.atlassian.com/get-started-with-sourcetree/install-sourcetree-847359094.html) will give us easy visual access to the sourcecode we will be using and allow us to check in our updates. 
This has already been done for you on your machine. You will need a Bitbucket account to install on your machine at home. (Choose BitBucket as opposed to 
BitBucket Server option).  

## Accounts Setup - Start Here!

### GitLab

1.  Sign into your [GitLab](https://gitlab.com) account or [create one](https://gitlab.com/users/sign_up) if you don't already have one. Make sure to remember these credentials. This is your software portfolio!
1.  Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). This will allow SourceTree to access GitLab 
1.  ![](assets/images/PAT0.png) 
1.  Connect your GitLab account to your SourceTree installation by copying the new PAT 
1. ![](assets/images/PAT01.png)
1.  Launch SourceTree. Go To Tools->Options->Authentication->Add 
1. ![](assets/images/PAT.png)
1.  Choose the Gitlab option, HTTPS, and hit refresh Personal Access Token. Enter your GitLabe username and the PAT from your clipboard
2.  ![](assets/images/PAT2.png)
1.  Fork the repo by going to [https://gitlab.com/staylornc/chicktechaiworkshopncsu](https://gitlab.com/staylornc/chicktechaiworkshopncsu). Hit the fork button and choose your namespace. A fork is a copy of a project.
Forking a repository allows you to make changes without affecting the original project. 
1.  You will be redirected to your fork of the rep. In your new repo...
1.  Copy the HTTPS Clone URL
1.  ![Gitlab](assets/images/Clone.png)
1.  Go to the SourceTree App.
1.  Hit "Clone" button, paste the URL for the source location, hit tab to cause the fields to be populated, and hit the clone button.
2.  ![](assets/images/Add_repo.png)
3.  This will cause all of the source code from your remote repository to be copied locally on your machine.
  
You have now made a copy of my sourcecode in your own GitLab repo. You have connected your new repo to the Windows tool, sourcetree. This tool will make it easier to make source code changes!




# Useful Links
[GitLab](https://gitlab.com) - Code repository. Continue to add projects here to create your portfolio  
[Anaconda](https://www.anaconda.com/distribution/) - Data Science Development Environment  
[Markdown Cheatsheet](https://www.markdownguide.org/basic-syntax/) - The language used for Readme files in your GitLab projects and Jupyter notebook text blocks
[SourceTree](https://www.sourcetreeapp.com/) - This is a source control client that will help you manange your projects

# Next Steps
1.  Complete the [Jupyter Notebook tutorial](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)
2.  Explore some of the tutorials on [Kaggle](https://www.kaggle.com/)
2.  Take a look at this [tutorial](https://www.datacamp.com/community/tutorials/recommender-systems-python)
2.  Perform the same exercise with a different dataset 
